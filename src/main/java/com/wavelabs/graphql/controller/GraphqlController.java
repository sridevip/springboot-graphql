package com.wavelabs.graphql.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.graphql.service.VehicleService;

@RestController
@RequestMapping
public class GraphqlController {

	@Autowired
	private VehicleService vehicleService;

	@PostMapping(value = "/graphql", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> addVehicleInformation(String type, String modelCode, String brandName, String launchDate) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(vehicleService.createVehicle(type, modelCode, brandName, launchDate));
	}
}
