package com.wavelabs.graphql.mutation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.wavelabs.graphql.dao.entity.Vehicle;
import com.wavelabs.graphql.service.VehicleService;

@Component
public class VehicleMutation implements GraphQLMutationResolver {

    @Autowired
    private VehicleService vehicleService;

    public Vehicle createVehicle(String type,String modelCode,String brandName,String launchDate) {
        return vehicleService.createVehicle(type, modelCode, brandName, launchDate);
    }
}
